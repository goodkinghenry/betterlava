This plugin for Spigot API 1.19 servers makes the products of quenching lava much more interesting and valuable. In addition, players not in Creative Mode will ignite rather than successfully filling a bucket with lava.

It comes with a configuration file that's fairly straightforward, for tweaking its behavior (product yields, etc.)

Current version: 0.2
