package digital.amaranth.mc.betterlava;

import digital.amaranth.mc.quickblocklib.Fluids.Fluids;
import digital.amaranth.mc.quickblocklib.Neighbors.Neighbors;

import java.util.Comparator;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.Location;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;

import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFormEvent;

public class BetterLavaProducts implements Listener {
    private final boolean ENABLED, NERFED;
    
    private final float LAVA_SIZZLE_VOLUME, LAVA_SIZZLE_PITCH;
    
    private final List<Map<?, ?>> SOURCE_PRODUCT_MAP_LIST,
            FLOWING_PRODUCT_MAP_LIST;
    
    private final Random r = new Random();
    
    public BetterLavaProducts (
            boolean enableBetterLavaProducts,
            List<Map<?, ?>> sourceProductMapList,
            boolean nerfBetterLavaProducts,
            List<Map<?, ?>> flowingProductMapList) {
        this.ENABLED = enableBetterLavaProducts;
        this.NERFED = nerfBetterLavaProducts;
        
        this.LAVA_SIZZLE_VOLUME = 6;
        this.LAVA_SIZZLE_PITCH = 3;
        
        this.SOURCE_PRODUCT_MAP_LIST = sourceProductMapList;
        this.FLOWING_PRODUCT_MAP_LIST = flowingProductMapList;
        
        sortLavaProducts();
    }
    
    /* sort lava products lists from least likely to most */
    private void sortLavaProducts () {
        Comparator <Map<?, ?>> sortByOddsAscending = (Map<?, ?> M1, Map<?, ?> M2) -> {
            int x1 = (Integer)M1.entrySet().iterator().next().getValue();
            int x2 = (Integer)M2.entrySet().iterator().next().getValue();
            
            if (x1 > x2) {
                return 1;
            } else {
                return -1;
            }
        };
        
        SOURCE_PRODUCT_MAP_LIST.sort(sortByOddsAscending);
        FLOWING_PRODUCT_MAP_LIST.sort(sortByOddsAscending);
    }
    
    private Material getLavaProduct (List<Map<?, ?>> mapList) {
        int oddsUsed = 0;
        
        for (var M : mapList) {
            for (var ES : M.entrySet()) {
                int absOdds = (Integer)ES.getValue();
                int relOdds = 100 * absOdds / (100 - oddsUsed);
                int roll = r.nextInt(0, 100);
                
                if (roll < relOdds) {
                    return Material.valueOf((String)ES.getKey());
                } else {
                    oddsUsed += absOdds;
                }
            }
        }
        
        return Material.COBBLESTONE; // if hell freezes over, you'll need this.
    }
    
    private Optional<Block> getWaterRelative (Block b) {
        return Neighbors.getDirectNeighbors(b).stream().
                filter(n -> n.getType().equals(Material.WATER)).
                findFirst();
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockFormEvent (BlockFormEvent e) {
        if (ENABLED) {
            Block b = e.getBlock();
            
            if (b.getType().equals(Material.LAVA)) {
                e.setCancelled(true);

                World w = b.getWorld();
                Location l = b.getLocation();

                w.playEffect(l.add(0, 2, 0), Effect.SMOKE, 0);
                w.playSound(l, Sound.BLOCK_LAVA_EXTINGUISH, LAVA_SIZZLE_VOLUME, LAVA_SIZZLE_PITCH);

                if (Fluids.isASourceBlock(b) || !NERFED) { 
                    b.setType(getLavaProduct(SOURCE_PRODUCT_MAP_LIST));
                } else {
                    b.setType(getLavaProduct(FLOWING_PRODUCT_MAP_LIST));
                }
                
                getWaterRelative(b).ifPresent(wb -> wb.setType(Material.AIR));
            }
        }
    }
}
