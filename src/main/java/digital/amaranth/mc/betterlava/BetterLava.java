package digital.amaranth.mc.betterlava;

import org.bukkit.Bukkit;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

import org.bukkit.plugin.java.JavaPlugin;

public class BetterLava extends JavaPlugin {
    public static BetterLava getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("BetterLava");
        if (plugin == null || !(plugin instanceof BetterLava)) {
            throw new RuntimeException("'BetterLava' not found.");
        }
        
        return ((BetterLava) plugin);
    }
    
    @Override
    public void onEnable() {
        saveDefaultConfig();
        
        var c = this.getConfig();
        
        getServer().getPluginManager().registerEvents(
                new BucketUse(
                        c.getBoolean("NerfLavaScooping"),
                        c.getInt("LavaScoopingFireTicks")
                ),
                this);
        getServer().getPluginManager().registerEvents(
                new BetterLavaProducts(
                        c.getBoolean("EnableBetterProducts"),
                        c.getMapList("ProductsList"),
                        c.getBoolean("NerfBetterProducts"),
                        c.getMapList("NonSourceProductsList")
                ), 
                this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}

