package digital.amaranth.mc.betterlava;

import digital.amaranth.mc.quickblocklib.Players.GameModes;

import org.bukkit.Material;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import org.bukkit.event.player.PlayerBucketFillEvent;

public class BucketUse implements Listener {  
    final boolean ENABLED;
    
    final int LAVA_FIRE_TICKS;
    
    public BucketUse (
            boolean nerfLavaScooping, 
            int lavaFireTicks) {
        this.ENABLED = nerfLavaScooping;
        this.LAVA_FIRE_TICKS = lavaFireTicks;
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onPlayerBucketFillEvent (PlayerBucketFillEvent e) {
        var p = e.getPlayer();
        
        if (ENABLED && 
                !GameModes.isInCreativeMode(p) && 
                e.getBlockClicked().getType().equals(Material.LAVA)) {
            p.setFireTicks(LAVA_FIRE_TICKS);
            
            e.setCancelled(true);
        }
    }
}
